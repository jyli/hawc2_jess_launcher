import click
from pathlib import Path
from tqdm import tqdm
from wetb.hawc2.htc_file import HTCFile
from wetb.utils.cluster_tools.pbsfile import PBSMultiRunner
from wetb.hawc2.hawc2_pbs_file import JESS_WINE32_HAWC2MB, wine_cmd

pbs_dir = Path("pbs_in")
num_nodes = 2
total_walltime = "08:00:00"

version = "v12.8.0.0"
platform = "win32"
hawc2_path = "/mnt/aiolos/groups/hawc2sim/HAWC2/%s/%s/" % (version, platform)
hawc2_cmd = wine_cmd(platform="win32", hawc2="hawc2mb.exe", cluster="jess")


@click.command()
@click.argument("htc_dir", type=click.Path(exists=True))
@click.option("-n", "--nodes", default=1, help="Number of nodes (default: 1)")
@click.option(
    "-ppn", "--ppn", default=20, help="Number of processors per node (default: 20)"
)
@click.option(
    "-T",
    "--totalwalltime",
    default="00:59:00",
    help="Total walltime (HH:MM:SS) (default: 00:59:00)",
)
@click.option(
    "-t",
    "--walltime",
    default="00:59:00",
    help="Individual walltime (HH:MM:SS) (default: 00:59:00)",
)
def launch(htc_dir, nodes, ppn, walltime, totalwalltime):
    """
    hawc2launch launches HAWC2 simulations. Run this script in the HAWC2 project
    working directory.
    """
    htc_dir = Path(htc_dir)
    pbs_dir.mkdir(parents=True, exist_ok=True)
    # Empty pbs_in directory
    print("Removing old PBS files...")
    for file in pbs_dir.iterdir():
        file.unlink()

    # Count htc files
    N = len(list(htc_dir.glob("*")))

    # Generate pbs files
    print("generating {N} PBS files...".format(N=N))

    for file in tqdm(htc_dir.iterdir(), total=N):
        htc = HTCFile(file.resolve().as_posix())
        pbs = htc.pbs_file(hawc2_path, hawc2_cmd, queue="workq", walltime=walltime,)

        pbs.save()
    print("\nDone.")

    # Generate PBS multirunner
    print("generating multirunner...", end="")
    multirunner = PBSMultiRunner(
        queue="workq",
        workdir=pbs.workdir,
        walltime=totalwalltime,
        nodes=nodes,
        ppn=ppn,
    )
    multirunner.save()
    print("Done.")

    print("\nTo launch, execute the following command on Jess:")
    print(f"qsub pbs_multirunner.all")
