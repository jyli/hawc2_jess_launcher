## HAWC2 Jess Launcher
You have a HAWC2 wind turbine model, and a bunch (1 to 100,000) of htc files. You would like to run those on the Jess cluster on some number of nodes (lets say 3). How do you do it? 

First, move to the HAWC2 model working directory:
```
cd path/to/working/directory
```
Then:
```
hawc2launch path/to/htc/files --nodes 3
``` 
Want more help? no worries:
```
hawc2launch --help
``` 

![](img/screencapture.gif)
## Installation
Clone this repository, then pip install (don't forget the '.' at the end):
```
git clone https://gitlab.windenergy.dtu.dk/jyli/hawc2_jess_launcher.git
cd hawc2_jess_launcher
pip install .
``` 