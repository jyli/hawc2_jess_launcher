# https://python-packaging.readthedocs.io
from setuptools import setup

setup(name                 = 'hawc2_jess_launcher',
      version              = '0.1',
      description          = 'a CLI tool launch HAWC2 simulations',
      #url                  = 
      author               = 'Jaime Liew',
      author_email         = 'jyli@dtu.dk',  
      packages             = ['hawc2_jess_launcher'],
      install_requires     = ['click', 'jinja2', 'wetb'],
      entry_points         = {
        'console_scripts': ['hawc2launch=hawc2_jess_launcher.hawc2_jess_launcher:launch']},
)
